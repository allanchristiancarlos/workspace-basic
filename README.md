# Workspace Basic #

## Features ##
- Oh my zsh terminal
- git
- apache
- php
- phpmyadmin
- Suitable for WordPress, Magento, and Laravel sites.

## Requirements ##
1. [Virtual Box](https://www.virtualbox.org/)
2. [Vagrant](vagrantup.com) 
3. workspace/basic vagrant box. Ask allan for the box
4. vagrant hostsupdater plugin
5. git

## Installation (One time ONLY) ##
- path terminal to directory.
- clone this repo

```
#!shell
git clone https://allanchristiancarlos@bitbucket.org/allanchristiancarlos/workspace-basic.git
```
- path to repo 
```
#!shell
cd /path/to/repo
```
- install vagrant hostsupdater
```
#!shell
vagrant plugin install vagrant-hostsupdater
```
- run vagrant
```
#!shell
vagrant up
```

## Everyday workflow ##
- path to workspace folder
```
#!shell
cd /path/to/workspace
```
- start server
```
#!shell
vagrant up --provision
```
- visit on browser [workspace.dev](http://workspace.dev) or workspace.dev/project

## Notes ##
- The **sites** folder is where you put all your projects.
- then visit workspace.dev 
- to add workspace/basic box
```
#!shell
vagrant box add workspace/basic /path/to/the/box/file
```